require('jasmine');
var add = require('./index.js');

// Test Suite
describe("Mon exemple de test NodeJS", function(){

     // Test Case
    it("Should return 10 when adding 5+5", function(){
        let actual =   add(5,5);  // On simule un traitement à tester
        let expected = 10;
        expect(actual).toEqual(expected);
    });
 });